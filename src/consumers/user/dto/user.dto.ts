import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { BaseDTO } from 'src/utils/base/base.dto';

export class UserDTO extends BaseDTO {
  @ApiProperty()
  username: string;
  
  @ApiProperty()
  password: string;

  @ApiProperty()
  fullName: string;

  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  phone: string;

  @ApiProperty()
  address: string;

  @ApiProperty()
  postCode: string;

  @ApiProperty()
  photo: string;

  @ApiProperty()
  cover: string;

  @ApiProperty()
  gender: string;

  @ApiProperty()
  religion: string;
}
