import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';
import { User } from 'proto-bundle/protoBundle';
import { UserDTO } from './dto/user.dto';
import { UserService } from './user.service';

@Controller('user')
@ApiBearerAuth()
@ApiTags('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  @ApiQuery({ name: 'pagination', required: false })
  @ApiQuery({ name: 'limit', required: false })
  async findAll(
    @Query('pagination') pagination: number,
    @Query('limit') limit: number,
  ) {
    const request: User.IRequest = {
      query: {
        pagination,
        limit,
      },
    };

    const result = await this.userService.findAll(request);
    return result;
  }

  @Get(':id')
  async find(@Param('id') id: string) {
    const request: User.IRequest = {
      params: {
        id,
      },
    };

    const result = await this.userService.find(request);
    return result;
  }

  @Post()
  async create(@Req() req: any, @Body() userDTO: UserDTO) {
    const { id } = req.user;
    const request: User.IRequest = {
      user: {
        id,
      },
      body: userDTO,
    };

    const result = await this.userService.create(request);
    return result;
  }

  @Put(':id')
  async update(
    @Req() req: any,
    @Param('id') id: string,
    @Body() userDTO: UserDTO,
  ) {
    const request: User.IRequest = {
      user: {
        id: req.user.id,
      },
      body: userDTO,
      params: { id },
    };

    const result = await this.userService.update(request);
    return result;
  }

  @Delete(':id')
  async delete(@Req() req: any, @Param('id') id: string) {
    const request: User.IRequest = {
      user: {
        id: req.user.id,
      },
      params: { id },
    };

    const result = await this.userService.delete(request);
    return result;
  }
}
