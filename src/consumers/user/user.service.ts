import { Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { DataCore, User } from 'proto-bundle/protoBundle';
import { from } from 'rxjs';
import { grpcDataCoreClientOptions } from 'src/configs/grpc.config';

@Injectable()
export class UserService implements OnModuleInit {
  @Client(grpcDataCoreClientOptions)
  private client: ClientGrpc;
  private greeter: DataCore.Greeter;

  onModuleInit() {
    this.greeter = this.client.getService<DataCore.Greeter>('Greeter');
  }

  async findAll(request: User.IRequest) {
    return from(this.greeter.users(request)).toPromise();
  }

  async find(request: User.IRequest) {
    return from(this.greeter.user(request)).toPromise();
  }

  async create(request: User.IRequest) {
    return from(this.greeter.userCreate(request)).toPromise();
  }

  async update(request: User.IRequest) {
    return from(this.greeter.userUpdate(request)).toPromise();
  }

  async delete(request: User.IRequest) {
    return from(this.greeter.userDelete(request)).toPromise();
  }
}
