import { ApiProperty } from '@nestjs/swagger';
import { BaseDTO } from 'src/utils/base/base.dto';

export class ProductImage {
  nameImage: string;
  altImage: string;
}

export class ProductCategory {
  categoryName: string;
  categorySlug: string;
}

export class ProductDTO extends BaseDTO {
  @ApiProperty()
  productName: string;

  @ApiProperty()
  productLogo: string;

  @ApiProperty()
  productDescription: string;

  @ApiProperty()
  productShortDescription: string;

  @ApiProperty()
  metaTitleProduct: string;

  @ApiProperty()
  metaDescriptionProduct: string;

  @ApiProperty()
  productSlug: string;

  @ApiProperty()
  metaKeyword: string;

  @ApiProperty()
  price: number;

  @ApiProperty({
    type: [ProductImage],
    example: [
      {
        nameImage: 'string',
        altImage: 'string',
      },
    ],
  })
  productImage: ProductImage[];

  @ApiProperty({ 
    type: [ProductCategory],
    example: [{
      categoryName: "string",
      categorySlug: "string"
    }]
  })
  productCategory: ProductCategory[];
}
