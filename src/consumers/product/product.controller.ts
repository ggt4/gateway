import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Product } from 'proto-bundle/protoBundle';
import { ProductDTO } from './dto/product.dto';
import { ProductService } from './product.service';

@Controller('product')
@ApiBearerAuth()
@ApiTags('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get()
  @ApiQuery({ name: 'pagination', required: false })
  @ApiQuery({ name: 'limit', required: false })
  async findAll(
    @Query('pagination') pagination: number,
    @Query('limit') limit: number,
  ) {
    const request: Product.IRequest = {
      query: {
        pagination,
        limit,
      },
    };

    const result = await this.productService.findAll(request);
    return result;
  }

  @Get(':id')
  async find(@Param('id') id: string) {
    const request: Product.IRequest = {
      params: {
        id,
      },
    };

    const result = await this.productService.find(request);
    return result;
  }

  @Post()
  async create(@Req() req: any, @Body() productDTO: ProductDTO) {
    const { id } = req.user;
    // const 
    const request: Product.IRequest = {
      user: {
        id,
      },
      body: productDTO
    }

    const result = await this.productService.create(request);
    return result;
  }

  @Put(':id')
  async update(
    @Req() req: any,
    @Param('id') id: string,
    @Body() productDTO,
  ) {
    const request: Product.IRequest = {
      user: {
        id: req.user.id,
      },
      body: productDTO as Product.Request.Body,
      params: { id },
    };

    const result = await this.productService.update(request);
    return result;
  }

  @Delete(':id')
  async delete(@Req() req: any, @Param('id') id: string) {
    const request: Product.IRequest = {
      user: {
        id: req.user.id,
      },
      params: { id },
    };

    const result = await this.productService.delete(request);
    return result;
  }
}
