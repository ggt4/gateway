import { Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { Catalogue, DataCore, Product } from 'proto-bundle/protoBundle';
import { from } from 'rxjs';
import { grpcCatalogueClientOptions } from 'src/configs/grpc.config';

@Injectable()
export class ProductService implements OnModuleInit {
  @Client(grpcCatalogueClientOptions)
  private client: ClientGrpc;
  private greeter: Catalogue.Greeter;

  onModuleInit() {
    this.greeter = this.client.getService<Catalogue.Greeter>('Greeter');
  }

  async findAll(request: Product.IRequest) {
    return from(this.greeter.products(request)).toPromise();
  }

  async find(request: Product.IRequest) {
    return from(this.greeter.product(request)).toPromise();
  }

  async create(request: Product.IRequest) {
    return from(this.greeter.productCreate(request)).toPromise();
  }

  async update(request: Product.IRequest) {
    return from(this.greeter.productUpdate(request)).toPromise();
  }

  async delete(request: Product.IRequest) {
    return from(this.greeter.productDelete(request)).toPromise();
  }
}
