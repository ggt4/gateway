import { Body, Controller, Get, Post, Req, Request } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Auth } from 'proto-bundle/protoBundle';
import { ProtobufTypes } from 'src/utils/protobuf-types';
import { AuthService } from './auth.service';
import { AuthDTO } from './dto/auth.dto';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(@Body() authDTO: AuthDTO) {
    const request: Auth.ILoginRequest = {
        body: {
          username: authDTO.username,
          password: authDTO.password,
      },
    };
    const result = await this.authService.login(request);
    return result;
  }

  @ApiBearerAuth()
  @Post('logout')
  async logout(@Request() req) {
    const authHeaders = req.headers.authorization;
    let token = '';
    if (authHeaders && (authHeaders as string).split(' ')[1]) {
      token = (authHeaders as string).split(' ')[1];
    }
    const request: Auth.ILogoutRequest = {};
    const result = await this.authService.logout(request);
    return result
  }

  @ApiBearerAuth()
  @Post('refresh-token')
  async refresToken(@Request() req) {
    const authHeaders = req.headers.authorization;
    let token = '';
    if (authHeaders && (authHeaders as string).split(' ')[1]) {
      token = (authHeaders as string).split(' ')[1];
    }
    const request: Auth.IRefreshTokenRequest = {
      message: {
        body: {
          token: token,
        },
      },
    };
    const result = await this.authService.refreshToken(request);
    return result;
  }
}
