import { Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { Auth, DataCore } from 'proto-bundle/protoBundle';
import { from } from 'rxjs';
import { grpcDataCoreClientOptions } from 'src/configs/grpc.config';

@Injectable()
export class AuthService implements OnModuleInit {
  @Client(grpcDataCoreClientOptions)
  private client: ClientGrpc;
  private greeter: DataCore.Greeter;

  onModuleInit() {
    this.greeter = this.client.getService<DataCore.Greeter>('Greeter');
  }

  async login(request: Auth.ILoginRequest) {
    return from(this.greeter.login(request)).toPromise();
  }

  async logout(request: Auth.ILogoutRequest) {
    return from(this.greeter.logout(request)).toPromise();
  }

  async refreshToken(request: Auth.IRefreshTokenRequest) {
    return from(this.greeter.refreshToken(request)).toPromise();
  }

  async checkToken(request: Auth.ILoginRequest) {
    return from(this.greeter.checkToken(request)).toPromise();
  }
}
