import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerMiddleware } from './middlewares/logger.middleware';
import * as dotenv from 'dotenv';
import { AuthService } from './consumers/auth/auth.service';
import { UserMicroModule } from './consumers/user/user.module';
import { AuthMicroModule } from './consumers/auth/auth.module';
import { ProductMicroModule } from './consumers/product/product.module';
dotenv.config();

@Module({
  imports: [
    UserMicroModule,AuthMicroModule,ProductMicroModule
  ],
  controllers: [AppController],
  providers: [AppService, AuthService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .exclude(
        {
          path: `${process.env.BASE_PATH}/auth/login`,
          method: RequestMethod.POST,
        },
        {
          path: `${process.env.BASE_PATH}/auth/logout`,
          method: RequestMethod.POST,
        },
      )
      .forRoutes({
        path: `/**`,
        method: RequestMethod.ALL,
      });
  }
}
