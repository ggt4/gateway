import * as dotenv from 'dotenv';
dotenv.config();
export const serviceName: string = process.env.SERVICE_NAME;
export const serviceCode: string = process.env.SERVICE_CODE;
export const basePath: string = process.env.BASE_PATH;
export const datacoreServer: string = process.env.DATA_CORE_SERVER;
export const catalogueServer: string = process.env.CATALOGUE_SERVER;