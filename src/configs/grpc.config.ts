import { ClientOptions, Transport } from '@nestjs/microservices';
import { credentials } from 'grpc';
import { join } from 'path';
import * as dotenv from 'dotenv';
import { catalogueServer, datacoreServer } from './env.config';
dotenv.config();
const protoDir = join(__dirname, '../../', 'protobufs');

export const grpcDataCoreClientOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    url: datacoreServer,
    package: 'DataCore',
    protoPath: protoDir + '/data-core/data-core.proto',
    credentials: credentials.createInsecure(),
    loader: {
      keepCase: true,
      longs: String,
      enums: String,
      defaults: true,
      oneofs: true,
    },
  },
};

export const grpcCatalogueClientOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    url: catalogueServer,
    package: 'Catalogue',
    protoPath: protoDir + '/catalogue/catalogue.proto',
    credentials: credentials.createInsecure(),
    loader: {
      keepCase: true,
      longs: String,
      enums: String,
      defaults: true,
      oneofs: true,
    },
  },
};
