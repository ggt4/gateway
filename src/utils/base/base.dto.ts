import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';

export class BaseDTO {
  @ApiHideProperty()
  _id: string;

  @ApiHideProperty()
  public createdBy: number;

  @ApiHideProperty()
  public createdAt: any;

  @ApiHideProperty()
  public updatedAt: any;

  @ApiHideProperty()
  public deletedAt: any;

  @ApiProperty({ default: null })
  public additional: object;
}
