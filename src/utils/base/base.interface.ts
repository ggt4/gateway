import { Document } from 'mongoose';
export interface BaseInterface extends Document {
  id: string;
  additional?: any;
  createdBy: number;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}
