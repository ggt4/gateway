export class OperandComparison {
  public static compareOne(param1: any, param2: any, operand: any) {
    switch (operand) {
      case '==':
        return param1 == param2;
      case '<=':
        return param1 <= param2;
      case '>=':
        return param1 >= param2;
      case '<':
        return param1 < param2;
      case '>':
        return param1 > param2;
      default:
        return false;
    }
  }
}
