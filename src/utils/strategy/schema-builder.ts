import { Document } from 'mongoose';

export interface SchemaBuilder {
  initialize();
  create(doc: any): Promise<Document>;
  update(filter: any, doc: any): Promise<Document>;
  find(filter: any): Promise<Document[]>;
  findOne(filter: any): Promise<Document>;
}
