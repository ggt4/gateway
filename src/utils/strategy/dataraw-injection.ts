import { SchemaBuilder } from './schema-builder';
import * as mongoose from 'mongoose';
import * as dotenv from 'dotenv';

dotenv.config();

export class DatarawInjection implements SchemaBuilder {
  private collectionName: string;
  private connection: mongoose.Connection;
  private schema: mongoose.Schema;
  private ModelBuilder: mongoose.Model<any>;

  constructor(collectionName: string) {
    this.collectionName = collectionName;
    this.connection = mongoose.createConnection(process.env.MONGODB_DR_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    this.initialize();
  }

  initialize() {
    this.schema = new mongoose.Schema({}, { strict: false });
    this.ModelBuilder =
      this.connection.models[this.collectionName] ||
      this.connection.model(
        this.collectionName,
        this.schema,
        this.collectionName,
      );
  }

  async create(doc: any): Promise<mongoose.Document<any>> {
    const model = new this.ModelBuilder(doc);
    const data = await model.save();
    await this.connection.close();
    return data;
  }

  async find(filter: any): Promise<mongoose.Document<any>[]> {
    const data = await this.ModelBuilder.find(filter).exec();
    await this.connection.close();
    return data;
  }

  async findOne(filter: any): Promise<mongoose.Document<any>> {
    const data = await this.ModelBuilder.findOne(filter).exec();
    await this.connection.close();
    return data;
  }

  async update(filter: any, doc: any): Promise<mongoose.Document<any>> {
    const data = await this.ModelBuilder.findOneAndUpdate(filter, doc);
    await this.connection.close();
    return data;
  }
}
