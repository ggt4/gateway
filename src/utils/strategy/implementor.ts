import { Document, Types } from 'mongoose';
import { SchemaBuilder } from './schema-builder';

export class Implementor {
  private schemaBuilder: SchemaBuilder;

  public setSchemaBuilder(schemaBuilder: SchemaBuilder) {
    this.schemaBuilder = schemaBuilder;
  }

  public async create(doc: any): Promise<Document> {
    return await this.schemaBuilder.create(doc);
  }

  public async find(doc: any): Promise<Document[]> {
    return await this.schemaBuilder.find(doc);
  }

  public async findOne(doc: any): Promise<Document> {
    return await this.schemaBuilder.findOne(doc);
  }

  public async update(id: any, doc: any): Promise<Document> {
    return await this.schemaBuilder.update({ submissionId: Types.ObjectId(id) }, doc);
  }
  
}
