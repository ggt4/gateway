import { serviceCode } from 'src/configs/env.config';
import { ProtobufTypes } from './protobuf-types';

export interface ResponseInterface<T> {
  data: T | T[];
  meta?: any;
  message: string;
}

export class Response {
  public static error(response: { message: string }) {
    return {
      data: null,
      meta: {
        serviceCode: serviceCode,
      },
      success: false,
      message: response.message || response,
    };
  }

  public static errorGrpc(response) {
    const meta = response.meta || {};
    return {
      message: {
        data: null,
        meta: {
          ...meta,
          serviceCode: meta.serviceCode || serviceCode,
        },
        success: false,
        message: response,
      },
    };
  }

  public static success(response: { data: any; message: string; meta?: any }) {
    const meta = response.meta || {};
    return {
      data: response.data,
      meta: {
        ...meta,
        serviceCode: meta.serviceCode || serviceCode,
      },
      success: true,
      message: response.message,
    };
  }

  public static successGrpc(response: {
    data: any;
    message: string;
    meta?: any;
    success: boolean;
  }) {
    return {
      message: {
        data: Response.transform(response.data),
        meta: response.meta,
        success: response.success,
        message: response.message,
      },
    };
  }

  public static dataToStruct(datas: any[]) {
    return datas.map((data) => {
      return ProtobufTypes.structEndcode(JSON.parse(JSON.stringify(data)));
    });
  }

  public static transform(data: any) {
    if (data instanceof Array) {
      return data.map((dt) => {
        if (dt) {
          const id = dt._id;
          delete dt._id;
          delete dt.createdBy;
          delete dt.createdAt;
          delete dt.updatedAt;
          delete dt.deletedAt;
          delete dt.__v;
          return {
            id,
            ...dt,
          };
        }
      });
    } else if (data) {
      data.id = data._id;
      const dt = { ...data };
      delete data.__v;
      delete data._id;
      return dt;
    }
    return null;
  }
}
