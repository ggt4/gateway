import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
  Next,
  Req,
  Res,
} from '@nestjs/common';
import { Auth } from 'proto-bundle/protoBundle';
import { AuthService } from 'src/consumers/auth/auth.service';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  constructor(private readonly authService: AuthService) {}
  async use(@Req() req: any, @Res() res: any, @Next() next: any) {
    const authHeaders = req.headers.authorization;
    if (authHeaders && (authHeaders as string).split(' ')[1]) {
      const token = (authHeaders as string).split(' ')[1];

      /**
       * @WARNING
       * set "checkAuthOnly: false" on request.message.body
       * in here can slow down performance for whole sistem backbone !!!
       */
      const request: Auth.ILoginRequest = {
          body: {
            token: 'Bearer '+ token,
          },
      };

      const { success, message, data } = await this.authService.checkToken(request);
      if (!success) {
        throw new HttpException(message, HttpStatus.UNAUTHORIZED);
      }

      req.user = data;
      next();
    } else {
      throw new HttpException('Not authorized.', HttpStatus.UNAUTHORIZED);
    }
  }
}
