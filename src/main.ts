import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import { join } from 'path';
import { AppModule } from './app.module';
import { basePath } from './configs/env.config';

const host: string = process.env.GRPC_HOST || '0.0.0.0';
const port: number = +process.env.PORT || 3000;

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bodyParser: true,
  });
  app.enableCors();
  app.setGlobalPrefix(process.env.BASE_PATH);
  const config = new DocumentBuilder()
    .setTitle('Form Builder API Docs')
    .setVersion('1.0')
    .setBasePath(process.env.BASE_PATH)
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  const customOptions: SwaggerCustomOptions = {
    swaggerOptions: {
      persistAuthorization: true,
    },
  };
  SwaggerModule.setup(
    basePath + `/explorer`,
    app,
    document,
    customOptions,
  );

  console.log(basePath + `/explorer`)

  await app.listen(port);
}
(async () => await bootstrap())();
