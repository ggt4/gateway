const program = require('commander');
const fsPromises = require('fs').promises;

program
  .command('update-env')
  .option('-e, --env <type>', 'env server')
  .action((options) => {
    let source = '.env.example'

    const sourceLocalDev = '.env.local-dev';
    const destination = '.env';

    if (options.env === 'LOCAL_DEV') {
      source = sourceLocalDev;
    }

    console.log('File Copying...');
    fsPromises
      .copyFile(source, destination)
      .then(function () {
        console.log('File Copied...');
      })
      .catch(function (error) {
        console.log('File NOT Copied');
      });
  });

program.parse(process.argv);
